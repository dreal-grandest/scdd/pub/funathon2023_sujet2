#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI
ui <- fluidPage(
  leafletOutput("map", height = 800),
  numericInput("buffer_radius", "Rayon (en km) :", value = 5),
  tableOutput("table")
)