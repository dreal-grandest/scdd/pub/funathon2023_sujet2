#

library(shiny)
library(leaflet)

# inutile de l'appeler. localisé dans un sous dossier appelé "R" :
# est appelé automatiquement
# source(file = "R/visu_rpg/zutils.R") 

# coordonnées initiales de la carte : Cleebourg (67)
zlat <- 49.004558014181264
zlon <- 7.893374288193224


# Définition du serveur
server <- function(input, output) {
  
  # Rendre la carte
  output$map <- renderLeaflet({
    leaflet() %>%
      addTiles("http://wxs.ign.fr/essentiels/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}") %>%
      setView(lng = zlon, lat = zlat, zoom = 12)
  })
  
  # Connexion à la base de données
  message("# Connexion à la base de données")
  cnx <- connect_to_db()
  
  # Initialisation d'une "reactive value" pour le point sélectionné
  selectedPoint <- reactiveValues(lat = NULL, lng = NULL)
  
  # Gestion de l'évènement "clic"
  observeEvent(input$map_click, {
    message("observeEvent(input$map_click)")
    clickData <- input$map_click
    if (!is.null(clickData)) {
      # Stockage des coordonnées du point
      message("Stockage des coordonnées du point")
      selectedPoint$lat <- clickData$lat
      selectedPoint$lng <- clickData$lng
      
      buffer_radius <- input$buffer_radius
      message("Appel de la fonction query_db()")
      sf <- query_db(cnx, selectedPoint$lat, selectedPoint$lng, buffer_radius)
      message("rayon = ", buffer_radius)
      # s_lat <- selectedPoint$lat
      # s_lng <- selectedPoint$lng
      message("s_lat = ", selectedPoint$lat)
      message("s_lng = ", selectedPoint$lng)
      # Mise à jour de la carte
      leafletProxy("map") %>%
        clearMarkers() %>%
        clearShapes() %>%
        addMarkers(lng = selectedPoint$lng, lat = selectedPoint$lat) %>%
        plot_surroundings(sf) %>% 
        addCircles(data = selectedPoint, 
                   lng = selectedPoint$lng, lat = selectedPoint$lat, 
                   radius = buffer_radius * 1000, # conversion km -> m
                   weight = 5,
                   color = "red",
                   fill = FALSE # permet la transparence pour afficher les markers
                   )
      
      # Calculs sur les données de parcelles
      df <- compute_stats(sf)
      
      # Update de la table affichée
      output$table <- renderTable({
        df
      })
    }
  })
  
  observeEvent(input$buffer_radius, {
    message("observeEvent(input$buffer_radius)")
    # Vérification qu'un point a été sélectionné
    if (!is.null(selectedPoint$lat) && !is.null(selectedPoint$lng)) {
      # Requête avec le nouveau rayon
      buffer_radius <- input$buffer_radius
      message("rayon = ", buffer_radius)
      s_lat <- selectedPoint$lat
      s_lng <- selectedPoint$lng
      message("s_lat = ", s_lat)
      message("s_lng = ", s_lng)
      sf <- query_db(cnx, s_lat, s_lng, buffer_radius)
      
      # Mise à jour de la carte
      message("Mise à jour de la carte")
      leafletProxy("map") %>%
        clearShapes()  %>%
        plot_surroundings(sf) %>% 
        addCircles(data = selectedPoint, 
                   lng =s_lng, lat = s_lat, 
                   # ~lng, ~lat, 
                   radius = buffer_radius * 1000, 
                   weight = 5,
                   color = "red", fillOpacity = 0)
      
      # Calculs sur les données de parcelles
      message("Calculs sur les données de parcelles")
      df <- compute_stats(sf)
      
      # Update de la table affichée
      output$table <- renderTable({
        df
      })
    }
  })
}