---
title: "evol_date_recolte"
output: html_document
date: "2023-06-28"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Généralités
Estimer les tendances de la date potentielle de récolte du maïs grain dans les différents bassins de productions français depuis 1980.  

Compétences mises en œuvre

-    Utiliser le langage R.
-    Manipuler des données géographiques : vecteurs et rasters avec les packages {sf}, {terra}, {stars}.
-    Requêter en SQL sur PostgreSQL/PostGIS avec {RPostgres} et {sf}.
-    Utiliser le lissage spatial pour synthétiser les données géographiques avec {btb}.
-    Créer une carte web interactive avec {leaflet}.
-    Estimer et visualiser des tendances : modèle linéaire simple et {ggplot2}.


## Données utilisées  

    Le RPG recense les parcelles déclarées à la PAC (Politique agricole commune) par les agricuteurs : données graphiques et leur culture principale.

    Les données 2021 (environ 10 millions de parcelles) sont disponibles dans une base PostgreSQL/PostGIS.

    ERA5 agrometeorological indicators est un jeu de données issu d’un projet de réanalyse météorologique qui vise à uniformiser et corriger les données historiques (ERA5). Il fournit des paramètres agro-météorologiques de surface quotidiens sur les quarante dernières années à une résolution spatiale de 0,1 ° (soit environ 8×11 km en France métropolitaine).

    Les données de température moyenne journalière 1979-2022 (87 Go à l’origine) ont été préchargées sur Minio et limitées à l’emprise de la métropole, représentant au final 16 000 fichiers de 76 ko = 1,1 Go).

    Elles sont au format NetCDF. Un package R existe pour importer et requêter ce jeu de données particulier : {ag5Tools}. Les données peuvent aussi être traitées avec les packages de manipulation de rasters tels que {terra} (ou {raster}) et {stars}.

    Admin Express est le référentiel des limites administratives de l’IGN. Des tables simplifiées des communes et des régions 2023 ont été intégrées dans la base PostgreSQL/PostGIS.


```{r init}
library(RPostgres)
library(dplyr)
library(aws.s3)
library(ggplot2)
library(raster)
library(sf)
library(raster)
library(janitor)
library(knitr)

source("R/ouvre_connexion.R")
```


## Données RPG


## Données de température ERA5

```{r}

# aws.s3::bucketlist(add_region = "")
# d_temperature <- terra::readValues(x = "../data/era5/1979/Temperature-Air-2m-Mean-24h_C3S-glob-agric_AgERA5_19790101_final-v1.0.nc")
library(ag5Tools)
d_temperature <- ag5_extract()

# coords = c(35.726364, -2.197162), 
#             dates = "1995-01-23", 
#             variable = "2m_temperature",
#             statistic = "Max-Day-Time", 
#             path = "C:/agera5_data")

```


